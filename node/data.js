var fs = require('fs');
var path = require('path');

var data;
var cities;

function Data() {
    // parse the main data file
    var filePath = path.join(__dirname, '..', 'data', 'climate-data.json');
    console.log('loading data from file' + filePath);
    data = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    console.log('loaded data for ' + data.length + ' cities');

    // sort the data into descending population
    data.sort(function(a, b) {
        return b.population - a.population;
    });
    
    // populate a city name array
    cities = [];
    for (var i = 0; i < data.length; i++) {
        cleanUpCityData(data[i]);
        cities.push(getCityDescription(data[i]));
        data[i].index = i;
    }
}

Data.prototype.getCities = function() {
    return cities;
}

Data.prototype.getCityData = function(cityIndex) {
    cityIndex = parseInt(cityIndex);
    if (isNaN(cityIndex) || cityIndex < 0 || cityIndex >= data.length) {
        return null;
    }
    return data[cityIndex];
}

module.exports = Data;

function cleanUpCityData(cityData) {
    if ("averageTemperatures" in cityData) {
        cleanUpData(cityData.averageTemperatures);
    }
    if ("minimumTemperatures" in cityData) {
        cleanUpData(cityData.minimumTemperatures);
    }
    if ("maximumTemperatures" in cityData) {
        cleanUpData(cityData.maximumTemperatures);
    }
}

function cleanUpData(arrayData) {
    for (var i = 0; i < arrayData.length; i++) {
        arrayData[i] = arrayData[i].toFixed(2);
    }
}

function getCityDescription(cityData) {
    var description = cityData.city;
    if (cityData.province != "" && cityData.province != cityData.city) {
        description += ", " + cityData.province;
    }
    description += ", " + cityData.country;
    return description;
}

