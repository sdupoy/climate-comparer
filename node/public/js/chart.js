var initialisationDataSet = prepareData([ 0, 0, 0, 30, 30, 30, 30, 30, 30, 0, 0, 0 ]);

var colours = [
    "steelblue",
    "red",
    "green",
    "blue",
    "orange",
];

var dataSets = [];
var width;
var svg;
var lineDefinition;
var yAxis;
var xScale;
var yScale;
var yRange = null;

function initialiseChart() {
    console.log("initialising chart");

    // dimensions
    var elementWidth = 800;
    var elementHeight = 400;
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 50,
    };
    width = elementWidth - margin.left - margin.right;
    var height = elementHeight - margin.top - margin.bottom;

    // axes scales
    xScale = d3.scaleTime().range([0, width]);
    yScale = d3.scaleLinear().range([height, 0]);

    // set an example range for the scales
    xScale.domain(d3.extent(initialisationDataSet, function(d) { return d.date; }));
    yScale.domain(d3.extent(initialisationDataSet, function(d) { return d.value; }));

    // build axes
    var xAxis = d3.axisBottom(xScale).tickFormat(d3.timeFormat("%b"));
    yAxis = d3.axisLeft(yScale);

    // a definition of how to build a line from our data sets
    lineDefinition =
        d3.line()
            .x(function(d) { return xScale(d.date); })
            .y(function(d) { return yScale(d.value); })
            .curve(d3.curveCatmullRom.alpha(0.5));

    // get the svg element we want to draw on
    svg = d3.select("#averageChart")
                .append("g")
                    .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");

    // add the x-axis to the chart
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, " + height + ")")
        .call(xAxis);

    // add the y-axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    console.log("initialised chart");
}

function addDataToChart(name, position, data) {
    console.log("adding data set to chart: " + name);
    
    // cache and prepare the data set
    data = cleanData(data, position);
    dataSets.push(data);
    data = prepareData(data);

    // draw the line
    var colour = colours[(dataSets.length - 1) % colours.length];
    console.log("colour: " + colour);
    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("stroke", colour)
        .attr("d", lineDefinition);
    
    // add a label
    var endDataPoint = data[data.length - 1];
    svg.append("text")
        .datum(endDataPoint)
        .attr("class", "label")
        .attr("transform", positionLabel) 
        .attr("dy", ".35em") // vertical center
        .attr("text-anchor", "start")
        .style("fill", colour)
        .text(name);

    // rescale the y-axis if necessary
    rescaleYAxis();
}

function cleanData(data, position) {
    // ensure the data points aren't strings
    data = data.map(function(value) {
        return typeof(value) == "string" ? parseFloat(value) : value;
    });

    // if the position indicates the city is in the southern hemisphere
    // then shift the data by 6 months
    if (position.latitude < 0.0) {
        console.log("rotating southern hemisphere data by 6 months");
        data = data.slice(6, 12).concat(data.slice(0, 6));
    }

    return data;
}

function prepareData(data) {
    return data.map(function(value, index) {
        return {
            date: new Date(2000, index, 1),
            value: value,
        };
    });
}

function rescaleYAxis() {
    var newRange = getRange();
    if (yRange !== null && newRange[0] == yRange[0] && newRange[1] == yRange[1]) {
        // no change in range
        console.log("no y-axis change");
        return;
    }

    console.log("rescaling y-axis to: " + newRange[0] + "-" + newRange[1]);
    yRange = newRange;
    yScale.domain(yRange);
    var transition = svg.transition().duration(500);
    transition.selectAll(".line").attr("d", lineDefinition)
    transition.selectAll(".label").attr("transform", positionLabel);
    transition.selectAll(".y.axis").call(yAxis);
}

function positionLabel(d) {
    return "translate(" + (xScale(d.date) + 3) + ", " + yScale(d.value) + ")";
}

function getRange() {
    var minimum = 0;
    var maximum = null;
    dataSets.forEach(function(dataSet) {
        dataSet.forEach(function(value) {
            minimum = value < minimum ? value : minimum;
            maximum = (maximum === null || value > maximum) ? value : maximum;
        });
    });
    maximum = maximum != null ? maximum : 30;
    return [ minimum, maximum ];
}

