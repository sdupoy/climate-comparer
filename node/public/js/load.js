var cities = null;
var citiesLookup = null;

function handleCities(data) {
    cities = data;
    console.log('preparing typeahead');
    
    var citiesTypeahead = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: cities
    });

    // build a lookup table. assume no duplicates but doesn't really
    // matter if there are
    citiesLookup = {};
    for (var i = 0; i < cities.length; i++) {
        citiesLookup[cities[i]] = i;
    }
    
    $("input#city").typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing result */
    },
    {
        name: 'cities',
        source: citiesTypeahead
    });
    
    console.log('typeahead ready');
    // todo: attach a handler to the click / select
}

function handleError(e) {
    log('ERROR!: ' + e.status);
    console.log(e);
}

function loadCities() {
    console.log("retrieving city data...");
    $.get('/api/cities', handleCities).fail(handleError);
}

function handleCityData(data) {
    console.log("received city data for: " + data.city);
    addDataToChart(data.city, data.position, data.averageTemperatures)
}

function addCity(event) {
    // prevent default action
    event.preventDefault();

    // prevent empty submission

    // prevent submission of non-listed value (maybe not here)

    // get text value
    var city = $("#city").val();
    
    // ensure the data exists
    if (!(city in citiesLookup)) {
        console.log("ERROR: unknown city: " + city);
        return;
    }

    // get the index for this city
    var cityIndex = citiesLookup[city];
    console.log("adding: " + city + " - " + cityIndex);
    $("#statuses").append('<div class="alert alert-info">add: ' + city + ' - ' + cityIndex + '</div>');

    // get city-specific data
    $.get("/api/city/" + cityIndex, handleCityData).fail(handleError);

    // reset form
    $("#city").val("");
}

$(function() {
    initialiseChart();
    loadCities();
    $("#addCountry").submit(addCity);
});

