function Routes() {
}

Routes.prototype.getHome = function() {
    return function(req, res) {
        res.render('home');
    };
}

Routes.prototype.getCities = function(data) {
    return function(req, res) {
        res.json(data.getCities());
    };
}

Routes.prototype.getCityData = function(data) {
    return function(req, res) {
        res.json(data.getCityData(req.params.id));
    };
}

module.exports = Routes;

