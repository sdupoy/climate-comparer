package com.dupoy.climate.datagatherer.Geometry;

import com.dupoy.climate.datagatherer.Data.Position;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DistanceCalculatorTest
{
    @Test
    public void calculateDistanceInKm() throws Exception
    {
        Position p1 = new Position(49.215367, -2.226877);
        Position p2 = new Position(49.201749, -2.017030);
        double distance = new DistanceCalculator().calculateDistanceInKm(p1, p2);
        assertTrue("distance should be > 15.3192km", distance > 15.3192);
        assertTrue("distance should be < 15.3193km", distance < 15.3193);
    }
}