package com.dupoy.climate.datagatherer.Download.Mappings;

import java.io.File;
import java.io.IOException;

public interface IMapping
{
    void DownloadAndUnpack()
            throws IOException;

    File getDestinationFile();
}
