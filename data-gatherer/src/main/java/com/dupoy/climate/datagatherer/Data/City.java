package com.dupoy.climate.datagatherer.Data;

public class City
{
    private String name;
    private String asciiName;
    private Position position;
    private int population;
    private Country country;
    private String province;

    public City(
            String name,
            String asciiName,
            Position position,
            int population,
            Country country,
            String province)
    {
        this.name = name;
        this.asciiName = asciiName;
        this.position = position;
        this.population = population;
        this.country = country;
        this.province = province;
    }

    public String getName()
    {
        return this.name;
    }

    public String getAsciiName()
    {
        return this.asciiName;
    }

    public Position getPosition()
    {
        return this.position;
    }

    public int getPopulation()
    {
        return this.population;
    }

    public Country getCountry()
    {
        return this.country;
    }

    public String getProvince()
    {
        return this.province;
    }
}
