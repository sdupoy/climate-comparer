package com.dupoy.climate.datagatherer.Gatherers.Ghncm;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.GathererException;

public class MonthlyDataSummer
{
    private Config config;
    private float[] dataSums;
    private int[] dataCounts;

    public MonthlyDataSummer(Config config)
    {
        this.config = config;
        this.dataSums = new float[12]; // 0 initialised
        this.dataCounts = new int[12]; // 0 initialised
    }

    public boolean isValid()
    {
        // check we have the minimum number of data points for each month (we don't mind if they're from
        // different years)
        int minimum = this.config.getMinimumRequiredYearsOfData();
        for (int dataCount : dataCounts)
        {
            if (dataCount < minimum)
            {
                return false;
            }
        }
        return true;
    }

    public int getCount()
    {
        int count = 0;
        for (int i = 0; i < 12; i++)
        {
            count += this.dataCounts[i];
        }
        return count;
    }

    public void addData(float[] data)
    {
        if (data.length != 12)
        {
            throw new GathererException("Cannot add monthly data which doesn't have 12 elements");
        }
        for (int i = 0; i < 12; i++)
        {
            float value = data[i];
            if (value > -99.0) // -99.99 is missing data magic number
            {
                this.dataSums[i] += value;
                this.dataCounts[i]++;
            }
        }
    }

    public Float[] getData()
    {
        Float[] data = new Float[12];
        for (int i = 0; i < 12; i++)
        {
            int count = this.dataCounts[i];
            data[i] = count == 0 ? null : (this.dataSums[i] / this.dataCounts[i]);
        }
        return data;
    }
}
