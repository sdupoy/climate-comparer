package com.dupoy.climate.datagatherer.Parsers;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.City;
import com.dupoy.climate.datagatherer.Data.Country;
import com.dupoy.climate.datagatherer.Data.Position;
import com.dupoy.climate.datagatherer.GathererException;
import com.dupoy.climate.datagatherer.Missing.IMissingLocation;
import com.dupoy.climate.datagatherer.Missing.MissingLocations;
import com.google.inject.Inject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CityFileParser
{
    private final Config config;
    private final CsvLineSplitter csvLineSplitter;
    private final MissingLocations missingLocations;

    @Inject
    public CityFileParser(
            Config config,
            CsvLineSplitter csvLineSplitter,
            MissingLocations missingLocations)
    {
        this.config = config;
        this.csvLineSplitter = csvLineSplitter;
        this.missingLocations = missingLocations;
    }

    public List<City> parseAndFilter(File file)
            throws IOException
    {
        List<City> cities = this.parseAndFilterFile(file);
        return this.addMissingLocations(cities);
    }

    private List<City> parseAndFilterFile(File file)
            throws IOException
    {
        System.out.println("Parsing city file: " + file.getPath());

        List<City> cities = new ArrayList<City>();
        BufferedReader reader = null;
        int filteredCount = 0;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String line;
            int row = 0;
            while ((line = reader.readLine()) != null)
            {
                row++;
                if (line.length() == 0)
                {
                    // skip blank lines
                    continue;
                }

                // split line and ensure it's the right length
                List<String> parts = this.csvLineSplitter.split(line);
                if (parts.size() != 9)
                {
                    throw new GathererException("line has unexpected number of parts: row: " + row + ", parts.size(): " + parts.size() + ", line: " + line);
                }

                // skip header line
                if (row == 1 && Objects.equals(parts.get(0), "city"))
                {
                    continue;
                }

                String name = parts.get(0);
                String asciiName = parts.get(1);
                String latitudeString = parts.get(2);
                String longitudeString = parts.get(3);
                String populationString = parts.get(4);
                String countryName = parts.get(5);
                String countryIsoShort = parts.get(6);
                String countryIsoLong = parts.get(7);
                String province = parts.get(8);

                double latitude = Double.parseDouble(latitudeString);
                double longitude = Double.parseDouble(longitudeString);
                Position position = new Position(latitude, longitude);

                Country country = new Country(countryName, countryIsoShort, countryIsoLong);

                int population = (int) Math.floor(Double.parseDouble(populationString));

                if (population < this.config.getMinimumCityPopulation())
                {
                    filteredCount++;
                    continue;
                }

                City city = new City(name, asciiName, position, population, country, province);
                cities.add(city);
            }
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
        }

        System.out.println("Parsed " + cities.size() + " cities, filtered out " + filteredCount + " cities due to low population");
        return cities;
    }

    public List<City> addMissingLocations(List<City> cities)
    {
        for (IMissingLocation missingLocation : this.missingLocations.getMissingLocations())
        {
            City missingCity = missingLocation.getCity();
            if (missingCity != null)
            {
                cities.add(missingCity);
            }
        }
        return cities;
    }
}
