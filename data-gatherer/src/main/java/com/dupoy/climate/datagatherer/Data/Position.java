package com.dupoy.climate.datagatherer.Data;

public class Position
{
    private double latitude;
    private double longitude;

    public Position(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude()
    {
        return this.latitude;
    }

    public double getLongitude()
    {
        return this.longitude;
    }
}
