package com.dupoy.climate.datagatherer.Missing;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Missing.Locations.Jersey;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

public class MissingLocations
{
    private final List<IMissingLocation> missingLocations;

    @Inject
    public MissingLocations(Config config)
    {
        this.missingLocations = new ArrayList<IMissingLocation>();
        this.missingLocations.add(new Jersey(config));
    }

    public List<IMissingLocation> getMissingLocations()
    {
        return this.missingLocations;
    }
}
