package com.dupoy.climate.datagatherer.Geometry;

import com.dupoy.climate.datagatherer.Data.Position;

public class DistanceCalculator
{
    private static double EarthDiameterInKm = 6371.0;
    private static double DegreeToRadianRatio = Math.PI / 180.0;

    public double calculateDistanceInKm(Position p1, Position p2)
    {
        // using haversine method. trig won't work as we near poles. could use vincenty method which is more accurate
        // but slower.
        double latDiff = this.convertToRadians(p2.getLatitude() - p1.getLatitude());
        double lonDiff = this.convertToRadians(p2.getLongitude() - p1.getLongitude());
        double sinLatDiff = Math.sin(latDiff / 2.0);
        double sinLonDiff = Math.sin(lonDiff / 2.0);
        double a = sinLatDiff * sinLatDiff + Math.cos(this.convertToRadians(p1.getLatitude())) * Math.cos(this.convertToRadians(p2.getLatitude())) * sinLonDiff * sinLonDiff;
        double sqrta = Math.sqrt(a);
        double c = 2.0 * Math.asin(1.0 < sqrta ? 1.0 : sqrta);
        return EarthDiameterInKm * c;
    }

    private double convertToRadians(double degrees)
    {
        return DegreeToRadianRatio * degrees;
    }
}
