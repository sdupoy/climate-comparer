package com.dupoy.climate.datagatherer;

import com.dupoy.climate.datagatherer.Config.DataGathererModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import java.io.IOException;

public class Main
{
    public static void main(String[] args)
            throws IOException
    {
        Injector injector = Guice.createInjector(new DataGathererModule());
        injector.getInstance(Controller.class).Run();
    }
}
