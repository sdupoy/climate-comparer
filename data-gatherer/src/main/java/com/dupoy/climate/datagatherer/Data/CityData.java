package com.dupoy.climate.datagatherer.Data;

import java.util.List;

public class CityData
{
    private final City city;
    private final List<CityDataSeries> dataSeries;

    public CityData(City city, List<CityDataSeries> dataSeries)
    {
        this.city = city;
        this.dataSeries = dataSeries;
    }

    public City getCity()
    {
        return city;
    }

    public List<CityDataSeries> getDataSeries()
    {
        return this.dataSeries;
    }
}
