package com.dupoy.climate.datagatherer.Download.Mappings;

import com.dupoy.climate.datagatherer.Config.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class SimpleMapping
    implements IMapping
{
    protected final Config config;
    protected final String sourcePath;
    protected final File destinationFile;

    public SimpleMapping(Config config, String sourcePath, String destinationFileName)
    {
        this.config = config;
        this.sourcePath = sourcePath;
        this.destinationFile = new File(this.config.getTempDirectoryPath(), destinationFileName);
    }

    public void DownloadAndUnpack()
            throws IOException
    {
        // download only. no need to unpack for a simple mapping.
        this.Download(this.sourcePath, this.destinationFile);
    }

    public File getDestinationFile()
    {
        return this.destinationFile;
    }

    protected void Download(String source, File destination)
            throws IOException
    {
        if (destination.exists())
        {
            System.out.println("Destination file already present: " + destination.getPath());
            return;
        }

        System.out.println("Downloading: " + source);
        ReadableByteChannel downloadChannel = null;
        FileOutputStream fileOutputStream = null;
        try
        {
            System.out.println("Downloading to: " + destination.getPath());
            downloadChannel = Channels.newChannel(new URL(source).openStream());
            fileOutputStream = new FileOutputStream(destination);
            fileOutputStream.getChannel().transferFrom(downloadChannel, 0, Long.MAX_VALUE);
            fileOutputStream.close();
            downloadChannel.close();
            System.out.println("Download complete");
        }
        finally
        {
            if (fileOutputStream != null)
            {
                fileOutputStream.close();
            }
            if (downloadChannel != null)
            {
                downloadChannel.close();
            }
        }
    }
}
