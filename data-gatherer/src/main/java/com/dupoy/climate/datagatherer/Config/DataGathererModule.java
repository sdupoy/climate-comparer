package com.dupoy.climate.datagatherer.Config;

import com.google.inject.AbstractModule;

public class DataGathererModule
        extends AbstractModule
{
    @Override
    public void configure()
    {
        // note: concrete implementations will be resolved without explicit bindings
        //bind(Controller.class).to(Controller.class);
    }
}
