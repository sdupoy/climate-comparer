package com.dupoy.climate.datagatherer.Parsers.Ghncm;

public class TemperatureDataRow
{
    private final String stationId;
    private final int year;
    private float[] data;

    public TemperatureDataRow(String stationId, int year, float[] data)
    {
        this.stationId = stationId;
        this.year = year;
        this.data = data;
    }

    public String getStationId()
    {
        return this.stationId;
    }

    public int getYear()
    {
        return this.year;
    }

    public float[] getData()
    {
        return this.data;
    }
}
