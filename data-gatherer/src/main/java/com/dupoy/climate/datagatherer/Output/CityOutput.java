package com.dupoy.climate.datagatherer.Output;

import com.dupoy.climate.datagatherer.Data.*;

import java.util.ArrayList;
import java.util.List;

/*
public class CityOutput
{
    private String city;
    private String province;
    private String country;
    private int population;
    private Position position;

    private String stationName;
    private Position stationPosition;
    private int stationElevation;

    private double distanceToStationInKm;

    private List<Float> averageTemperatures;
    private List<Float> maximumTemperatures;
    private List<Float> minimumTemperatures;

    public CityOutput(CityClimateData data)
    {
        City city = data.getCity();
        this.city = city.getName();
        this.province = city.getProvince();
        this.country = city.getCountry().getIsoNameLong();
        this.population = city.getPopulation();
        this.position = city.getPosition();

        StationClimateData stationClimateData = data.getStationClimateData();
        Station station = stationClimateData.getStation();
        this.stationName = station.getName();
        this.stationPosition = station.getPosition();
        this.stationElevation = station.getElevation();

        this.distanceToStationInKm = data.getDistanceInKm();

        this.averageTemperatures = this.getTemperatures(stationClimateData.getAverageTemperatures());
        this.maximumTemperatures = this.getTemperatures(stationClimateData.getMaximumTemperatures());
        this.minimumTemperatures = this.getTemperatures(stationClimateData.getMinimumTemperatures());
    }

    public String getCity()
    {
        return this.city;
    }

    public String getProvince()
    {
        return this.province;
    }

    public String getCountry()
    {
        return this.country;
    }

    public int getPopulation()
    {
        return this.population;
    }

    public Position getPosition()
    {
        return this.position;
    }

    public String getStationName()
    {
        return this.stationName;
    }

    public Position getStationPosition()
    {
        return this.stationPosition;
    }

    public int getStationElevation()
    {
        return this.stationElevation;
    }

    public double getDistanceToStationInKm()
    {
        return this.distanceToStationInKm;
    }

    public List<Float> getAverageTemperatures()
    {
        return this.averageTemperatures;
    }

    public List<Float> getMaximumTemperatures()
    {
        return this.maximumTemperatures;
    }

    public List<Float> getMinimumTemperatures()
    {
        return this.minimumTemperatures;
    }

    private List<Float> getTemperatures(MonthlyData data)
    {
        List<Float> results = new ArrayList<Float>();
        for (float dataPoint : data.getData())
        {
            // if there are any missing points then we don't return any data. this should only happen for the
            // manually added missing data sets if we're missing a type of data (min, max, etc).
            if (dataPoint < -99.0f)
            {
                // no data
                return new ArrayList<Float>();
            }
            results.add(dataPoint);
        }
        return results;
    }
}
*/
