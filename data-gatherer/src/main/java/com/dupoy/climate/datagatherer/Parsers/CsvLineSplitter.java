package com.dupoy.climate.datagatherer.Parsers;

import com.dupoy.climate.datagatherer.GathererException;

import java.util.ArrayList;
import java.util.List;

public class CsvLineSplitter
{
    public List<String> split(String line)
    {
        // need to handle quotes and trailing "," characters correctly
        List<String> parts = new ArrayList<String>(10);
        StringBuilder builder = new StringBuilder();
        String part = "";
        boolean inQuotes = false;
        for (int i = 0; i < line.length(); i++)
        {
            char c = line.charAt(i);

            if (c == '"')
            {
                if (!inQuotes)
                {
                    // must be at start of line
                    if (part.length() > 0)
                    {
                        throw new GathererException("Misplaced csv quotes, not at start of part");
                    }
                    inQuotes = true;
                }
                else
                {
                    // ensure that this is the end of the part if it's not the end of the line
                    if ((i + 1) < line.length() && line.charAt(i + 1) != ',')
                    {
                        throw new GathererException("Csv end quote is not at the end of the part");
                    }
                    inQuotes = false;
                }

                // don't add quotes onto part
            }
            else if (!inQuotes && c == ',')
            {
                // end of part and start new one
                parts.add(part);
                part = "";
            }
            else
            {
                part += c;
            }
        }

        // add the last part
        parts.add(part);

        return parts;
    }
}
