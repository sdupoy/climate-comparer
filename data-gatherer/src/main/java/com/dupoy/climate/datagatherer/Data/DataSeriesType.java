package com.dupoy.climate.datagatherer.Data;

public enum DataSeriesType
{
    AverageTemperature,

    MinimumTemperature,

    MaximumTemperature,

    Precipitation,

    AverageWind,

    MinimumRelativeHumidity,

    MaximumRelativeHumditiy,

    SnowDepth,

    SnowFall,

    DailyTotalSun,
}
