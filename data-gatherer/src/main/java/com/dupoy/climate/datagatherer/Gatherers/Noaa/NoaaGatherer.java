package com.dupoy.climate.datagatherer.Gatherers.Noaa;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.DataSeries;
import com.dupoy.climate.datagatherer.Data.DataSeriesType;
import com.google.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NoaaGatherer
{
    private final Config config;
    private final NoaaFileReader noaaFileReader;

    @Inject
    public NoaaGatherer(
            Config config,
            NoaaFileReader noaaFileReader)
    {
        this.config = config;
        this.noaaFileReader = noaaFileReader;
    }

    public List<DataSeries> gather()
            throws IOException
    {
        System.out.println("NoaaGatherer.gather()");

        // first build the station list, applying latitudes, longitudes and elevations
        this.noaaFileReader.retrieve1DIntegers(this.config.getNoaaElevationDataSource(), "elev", (NoaaDataPair<Integer> p) -> p.getStation().setElevation(p.getValue()));
        this.noaaFileReader.retrieve1DFloats(this.config.getNoaaLatitudeDataSource(), "lat", (NoaaDataPair<Float> p) -> p.getStation().setLatitude((double)p.getValue()));
        this.noaaFileReader.retrieve1DFloats(this.config.getNoaaLongitudeDataSource(), "lon", (NoaaDataPair<Float> p) -> p.getStation().setLongitude((double)p.getValue()));

        // now extract the data which will pull in the stations build above via the NoaaStations object
        List<DataSeries> data = new ArrayList<DataSeries>();
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaMaximumTemperatureDataSource(), "TMAX", DataSeriesType.MaximumTemperature));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaMinimumTemperatureDataSource(), "TMIN", DataSeriesType.MinimumTemperature));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaPrecipitationDataSource(), "PRCP", DataSeriesType.Precipitation));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaDailyTotalSunDataSource(), "TSUN", DataSeriesType.DailyTotalSun));
        data.addAll(this.noaaFileReader.retrieve2DBytes(this.config.getNoaaMinimumRelativeHumidityDataSource(), "MNRH", DataSeriesType.MinimumRelativeHumidity));
        data.addAll(this.noaaFileReader.retrieve2DBytes(this.config.getNoaaMaximumRelativeHumidityDataSource(), "MXRH", DataSeriesType.MaximumRelativeHumditiy));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaAverageDailyWindSpeedDataSource(), "AWND", DataSeriesType.AverageWind));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaSnowFallDataSource(), "SNOW", DataSeriesType.SnowFall));
        data.addAll(this.noaaFileReader.retrieve2DShorts(this.config.getNoaaSnowDepthDataSource(), "SNWD", DataSeriesType.SnowDepth));
        System.out.println("----------------------------------------------------------------------");
        System.out.println("TOOD: - normalise units (f => c (most important), inches => mm or cm");
        System.out.println("----------------------------------------------------------------------");
        return data;
    }
}
