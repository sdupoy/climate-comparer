package com.dupoy.climate.datagatherer.Gatherers.Noaa;

import com.dupoy.climate.datagatherer.Data.Station;

public class NoaaDataPair<T>
{
    private final Station station;
    private final T value;

    public NoaaDataPair(Station station, T value)
    {
        this.station = station;
        this.value = value;
    }

    public Station getStation()
    {
        return station;
    }

    public T getValue()
    {
        return value;
    }
}
