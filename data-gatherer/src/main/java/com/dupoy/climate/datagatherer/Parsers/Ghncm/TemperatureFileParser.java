package com.dupoy.climate.datagatherer.Parsers.Ghncm;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.GathererException;
import com.google.inject.Inject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TemperatureFileParser
{
    private final boolean filterOutRowsWithAnyMissingData = false;
    private final Config config;

    @Inject
    public TemperatureFileParser(Config config)
    {
        this.config = config;
    }

    public List<TemperatureDataRow> parseAndFilter(File file)
            throws IOException
    {
        System.out.println("Parsing temperature data file: " + file.getPath());

        List<TemperatureDataRow> rows = new ArrayList<TemperatureDataRow>();
        BufferedReader reader = null;
        int filteredRowCount = 0;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String line = null;
            int row = 0;
            while ((line = reader.readLine()) != null)
            {
                row++;

                // skip blank lines
                if (line.length() == 0)
                {
                    continue;
                }

                // ensure lines are the correct length
                if (line.length() != 115)
                {
                    throw new GathererException("temperature file line is wrong length, row: " + row + ", length: " + line.length() + ", line: " + line);
                }

                String stationId = line.substring(0, 11);
                String yearString = line.substring(11, 15);
                int year = Integer.parseInt(yearString);

                // check filter on year
                if (year < this.config.getEarliestYearToAcceptData())
                {
                    filteredRowCount++;
                    continue;
                }

                float[] data = new float[12];
                boolean missingData = false;
                for (int i = 0; i < 12; i++)
                {
                    int startIndex = 19 + i * 8;
                    String raw = line.substring(startIndex, startIndex + 5);
                    int scaledValue = Integer.parseInt(raw.trim());

                    // filter on scaledValue
                    if (this.filterOutRowsWithAnyMissingData && scaledValue == -9999)
                    {
                        missingData = true;
                        break;
                    }

                    data[i] = scaledValue / 100.0f;
                }

                // filter on missing (ie -9999) data
                if (missingData)
                {
                    filteredRowCount++;
                    continue;
                }

                rows.add(new TemperatureDataRow(stationId, year, data));
            }
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
        }

        System.out.println("Parsed " + rows.size() + " temperature data rows, filtered " + filteredRowCount + " rows");
        return rows;
    }
}
