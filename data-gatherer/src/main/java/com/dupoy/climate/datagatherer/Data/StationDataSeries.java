package com.dupoy.climate.datagatherer.Data;

import java.util.ArrayList;
import java.util.List;

public class StationDataSeries
{
    private final Station station;
    private final List<DataSeries> dataSeriesList;

    public StationDataSeries(Station station)
    {
        this.station = station;
        this.dataSeriesList = new ArrayList<DataSeries>();
    }

    public Station getStation()
    {
        return station;
    }

    public List<DataSeries> getDataSeriesList()
    {
        return dataSeriesList;
    }
}
