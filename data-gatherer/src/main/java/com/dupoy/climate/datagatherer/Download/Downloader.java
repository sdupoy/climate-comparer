package com.dupoy.climate.datagatherer.Download;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Download.Mappings.IMapping;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;

public class Downloader
{
    private final Config config;

    @Inject
    public Downloader(Config config)
    {
        this.config = config;
    }

    public void DownloadAndUnpack()
            throws IOException
    {
        System.out.println("Downloading files");
        this.EnsureDirectories();
        for (IMapping mapping : this.config.getDataSources())
        {
            mapping.DownloadAndUnpack();
        }
    }

    private void EnsureDirectories()
            throws IOException
    {
        File directory = new File(this.config.getTempDirectoryPath());
        if (!directory.exists())
        {
            System.out.println("Creating directory: " + directory.getAbsolutePath());
            directory.mkdir();
        }
    }
}
