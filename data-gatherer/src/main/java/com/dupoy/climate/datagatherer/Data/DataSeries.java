package com.dupoy.climate.datagatherer.Data;

public class DataSeries
{
    private final DataSeriesType type;
    private final Station station;
    private final Float[] data;
    private final int sourceCount;

    public DataSeries(
            DataSeriesType type,
            Station station,
            Float[] data,
            int sourceCount)
    {
        this.type = type;
        this.station = station;
        this.data = data;
        this.sourceCount = sourceCount;
    }

    public DataSeriesType getType()
    {
        return this.type;
    }

    public Station getStation()
    {
        return this.station;
    }

    public Float[] getData()
    {
        return this.data;
    }

    public int getSourceCount()
    {
        return sourceCount;
    }
}
