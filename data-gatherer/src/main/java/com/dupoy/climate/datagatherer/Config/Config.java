package com.dupoy.climate.datagatherer.Config;

import com.dupoy.climate.datagatherer.Download.Mappings.CompressedMapping;
import com.dupoy.climate.datagatherer.Download.Mappings.IMapping;
import com.dupoy.climate.datagatherer.Download.Mappings.SimpleMapping;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Config
{
    private final String tempDirectoryPath;
    private final File outputFile;
    private final IMapping cityDataSource;
    private final IMapping stationDataSource;
    private final IMapping averageTemperatureDataSource;
    private final IMapping minimumTemperatureDataSource;
    private final IMapping maximumTemperatureDataSource;

    private final IMapping noaaAverageDailyWindSpeedDataSource;
    private final IMapping noaaElevationDataSource;
    private final IMapping noaaLatitudeDataSource;
    private final IMapping noaaLongitudeDataSource;
    private final IMapping noaaMinimumRelativeHumidityDataSource;
    private final IMapping noaaMaximumRelativeHumidityDataSource;
    private final IMapping noaaPrecipitationDataSource;
    private final IMapping noaaSnowDepthDataSource;
    private final IMapping noaaMaximumTemperatureDataSource;
    private final IMapping noaaMinimumTemperatureDataSource;
    private final IMapping noaaDailyTotalSunDataSource;
    private final IMapping noaaSnowFallDataSource;

    private final List<IMapping> dataSources;
    private final int minimumRequiredYearsOfData;
    private final int earliestYearToAcceptData;
    private final int minimumCityPopulation;

    private final double maximumCityStationDistanceInKm;
    private final double preferredCityStationDistanceInKm;

    private final int noaaMinimumDailyDataCountPerMonth;

    public Config()
    {
        this.tempDirectoryPath = "/tmp/climate";
        this.outputFile = new File(this.tempDirectoryPath, "climate-data.json");
        this.cityDataSource = new SimpleMapping(this, "http://simplemaps.com/static/data/world-cities/basic/simplemaps-worldcities-basic.csv", "cities.csv");
        this.stationDataSource = new SimpleMapping(this, "https://data.giss.nasa.gov/gistemp/station_data/v3.temperature.inv.txt", "stations.txt");
        this.averageTemperatureDataSource = new CompressedMapping(this, "ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/v3/ghcnm.tavg.latest.qca.tar.gz", "ghcnm-avg.txt");
        this.minimumTemperatureDataSource = new CompressedMapping(this, "ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/v3/ghcnm.tmin.latest.qca.tar.gz", "ghcnm-min.txt");
        this.maximumTemperatureDataSource = new CompressedMapping(this, "ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/v3/ghcnm.tmax.latest.qca.tar.gz", "ghcnm-max.txt");

        this.noaaAverageDailyWindSpeedDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/AWND/data.nc", "noaa-awnd.nc");
        this.noaaElevationDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/elev/data.nc", "noaa-elev.nc");
        this.noaaLatitudeDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/lat/data.nc", "noaa-lat.nc");
        this.noaaLongitudeDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/lon/data.nc", "noaa-lon.nc");
        this.noaaMinimumRelativeHumidityDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/MNRH/data.nc", "noaa-mnrh.nc");
        this.noaaMaximumRelativeHumidityDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/MXRH/data.nc", "noaa-mxrh.nc");
        this.noaaPrecipitationDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/PRCP/data.nc", "noaa-prcp.nc");
        this.noaaSnowDepthDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/SNWD/data.nc", "noaa-snwd.nc");
        this.noaaMaximumTemperatureDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/TMAX/data.nc", "noaa-tmax.nc");
        this.noaaMinimumTemperatureDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/TMIN/data.nc", "noaa-tmin.nc");
        this.noaaDailyTotalSunDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/TSUN/data.nc", "noaa-tsun.nc");
        this.noaaSnowFallDataSource = new SimpleMapping(this, "http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/SNOW/data.nc", "noaa-snow.nc");

        this.dataSources = Arrays.asList(
                this.cityDataSource,
                this.stationDataSource,
                this.averageTemperatureDataSource,
                this.minimumTemperatureDataSource,
                this.maximumTemperatureDataSource,
                this.noaaAverageDailyWindSpeedDataSource,
                this.noaaElevationDataSource,
                this.noaaLatitudeDataSource,
                this.noaaLongitudeDataSource,
                this.noaaMinimumRelativeHumidityDataSource,
                this.noaaMaximumRelativeHumidityDataSource,
                this.noaaPrecipitationDataSource,
                this.noaaSnowDepthDataSource,
                this.noaaMaximumTemperatureDataSource,
                this.noaaMinimumTemperatureDataSource,
                this.noaaDailyTotalSunDataSource,
                this.noaaSnowFallDataSource);
        this.minimumRequiredYearsOfData = 1;
        this.earliestYearToAcceptData = 1950;
        System.out.println("REVERT POPULATION CHECK TO 5000");
        //this.minimumCityPopulation = 5000;
        this.minimumCityPopulation = 500000;

        this.maximumCityStationDistanceInKm = 100.0;
        this.preferredCityStationDistanceInKm = 25.0;

        this.noaaMinimumDailyDataCountPerMonth = 10;
    }

    public String getTempDirectoryPath()
    {
        return this.tempDirectoryPath;
    }

    public File getOutputFile()
    {
        return this.outputFile;
    }

    public IMapping getCityDataSource()
    {
        return this.cityDataSource;
    }

    public IMapping getStationDataSource()
    {
        return this.stationDataSource;
    }

    public IMapping getAverageTemperatureDataSource()
    {
        return this.averageTemperatureDataSource;
    }

    public IMapping getMinimumTemperatureDataSource()
    {
        return this.minimumTemperatureDataSource;
    }

    public IMapping getMaximumTemperatureDataSource()
    {
        return this.maximumTemperatureDataSource;
    }

    public List<IMapping> getDataSources()
    {
        return this.dataSources;
    }

    public int getMinimumRequiredYearsOfData()
    {
        return this.minimumRequiredYearsOfData;
    }

    public int getEarliestYearToAcceptData()
    {
        return this.earliestYearToAcceptData;
    }

    public int getMinimumCityPopulation()
    {
        return this.minimumCityPopulation;
    }

    public double getMaximumCityStationDistanceInKm()
    {
        return this.maximumCityStationDistanceInKm;
    }

    public double getPreferredCityStationDistanceInKm()
    {
        return preferredCityStationDistanceInKm;
    }

    public int getNoaaMinimumDailyDataCountPerMonth()
    {
        return this.noaaMinimumDailyDataCountPerMonth;
    }

    public IMapping getNoaaElevationDataSource()
    {
        return noaaElevationDataSource;
    }

    public IMapping getNoaaLatitudeDataSource()
    {
        return noaaLatitudeDataSource;
    }

    public IMapping getNoaaAverageDailyWindSpeedDataSource()
    {
        return noaaAverageDailyWindSpeedDataSource;
    }
    public IMapping getNoaaMinimumTemperatureDataSource()
    {
        return noaaMinimumTemperatureDataSource;
    }

    public IMapping getNoaaSnowFallDataSource()
    {
        return noaaSnowFallDataSource;
    }

    public IMapping getNoaaLongitudeDataSource()
    {
        return noaaLongitudeDataSource;
    }

    public IMapping getNoaaMinimumRelativeHumidityDataSource()
    {
        return noaaMinimumRelativeHumidityDataSource;
    }

    public IMapping getNoaaSnowDepthDataSource()
    {
        return noaaSnowDepthDataSource;
    }

    public IMapping getNoaaMaximumTemperatureDataSource()
    {
        return noaaMaximumTemperatureDataSource;
    }

    public IMapping getNoaaMaximumRelativeHumidityDataSource()
    {
        return noaaMaximumRelativeHumidityDataSource;
    }

    public IMapping getNoaaPrecipitationDataSource()
    {
        return noaaPrecipitationDataSource;
    }

    public IMapping getNoaaDailyTotalSunDataSource()
    {
        return noaaDailyTotalSunDataSource;
    }

}
