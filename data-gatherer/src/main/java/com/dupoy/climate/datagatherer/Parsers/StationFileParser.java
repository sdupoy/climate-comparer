package com.dupoy.climate.datagatherer.Parsers;

import com.dupoy.climate.datagatherer.Data.Position;
import com.dupoy.climate.datagatherer.Data.Source;
import com.dupoy.climate.datagatherer.Data.Station;
import com.dupoy.climate.datagatherer.GathererException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StationFileParser
{
    public List<Station> parse(File file)
            throws IOException
    {
        System.out.println("Parsing station file: " + file.getPath());

        List<Station> stations = new ArrayList<Station>();
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            boolean reachedData = false;

            int row = 0;
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                row++;

                // wait until all the header stuff is finished
                if (!reachedData)
                {
                    if (line.startsWith("iccWMO_#... Name"))
                    {
                        reachedData = true;
                    }
                    continue;
                }

                // skip blank lines
                if (line.length() == 0)
                {
                    continue;
                }

                // assert that the line length is correct
                if (line.length() != 106)
                {
                    throw new GathererException("station file row " + row + " is wrong length");
                }

                String id = line.substring(0, 11).trim();
                String name = line.substring(12, 42).trim();
                String latitudeString = line.substring(43, 49).trim();
                String longitudeString = line.substring(50, 57).trim();
                String elevationString = line.substring(58, 62).trim();

                double latitude = Double.parseDouble(latitudeString);
                double longitude = Double.parseDouble(longitudeString);
                int elevation = Integer.parseInt(elevationString);

                Station station = new Station(Source.Ghcnm, id, name);
                station.setLatitude(latitude);
                station.setLongitude(longitude);
                station.setElevation(elevation);
                stations.add(station);
            }
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
        }

        System.out.println("Parsed " + stations.size() + " stations");
        return stations;
    }
}
