package com.dupoy.climate.datagatherer.Data;

public class Country
{
    private String name;
    private String isoNameShort;
    private String isoNameLong;

    public Country(String name, String isoNameShort, String isoNameLong)
    {
        this.name = name;
        this.isoNameShort = isoNameShort;
        this.isoNameLong = isoNameLong;
    }

    public String getName()
    {
        return this.name;
    }

    public String getIsoNameShort()
    {
        return this.isoNameShort;
    }

    public String getIsoNameLong()
    {
        return this.isoNameLong;
    }
}
