package com.dupoy.climate.datagatherer.Gatherers;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.*;
import com.dupoy.climate.datagatherer.Gatherers.Ghncm.GhncmGatherer;
import com.dupoy.climate.datagatherer.Gatherers.Noaa.NoaaGatherer;
import com.dupoy.climate.datagatherer.Parsers.CityFileParser;
import com.google.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Gatherer
{
    private final Config config;
    private final GhncmGatherer ghncmGatherer;
    private final NoaaGatherer noaaGatherer;
    private final CityFileParser cityFileParser;
    private final CityDataSeriesMatcher cityDataSeriesMatcher;

    @Inject
    public Gatherer(
            Config config,
            GhncmGatherer ghncmGatherer,
            NoaaGatherer noaaGatherer,
            CityFileParser cityFileParser,
            CityDataSeriesMatcher cityDataSeriesMatcher)
    {
        this.config = config;
        this.ghncmGatherer = ghncmGatherer;
        this.noaaGatherer = noaaGatherer;
        this.cityFileParser = cityFileParser;
        this.cityDataSeriesMatcher = cityDataSeriesMatcher;
    }

    public List<CityData> gather()
            throws IOException
    {
        System.out.println("Gatherer.gather()");
        List<DataSeries> data = this.getDataSeries();
        return this.matchCitiesToDataSeries(data);
    }

    private List<DataSeries> getDataSeries()
            throws IOException
    {
        System.out.println("Gatherer.getDataSeries()");
        List<DataSeries> data = new ArrayList<DataSeries>();
        data.addAll(this.ghncmGatherer.gather());
        data.addAll(this.noaaGatherer.gather());
        System.out.println("Gathered " + data.size() + " data series");
        return data;
    }

    private List<CityData> matchCitiesToDataSeries(List<DataSeries> data)
            throws IOException
    {
        System.out.println("Gatherer.matchCitiesToDataSeries()");
        List<City> cities = this.cityFileParser.parseAndFilter(this.config.getCityDataSource().getDestinationFile());
        return this.cityDataSeriesMatcher.match(cities, data);
    }
}
