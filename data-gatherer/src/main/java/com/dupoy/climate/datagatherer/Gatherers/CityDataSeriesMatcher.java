package com.dupoy.climate.datagatherer.Gatherers;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.*;
import com.dupoy.climate.datagatherer.Geometry.DistanceCalculator;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CityDataSeriesMatcher
{
    private final Config config;
    private final DistanceCalculator distanceCalculator;

    @Inject
    public CityDataSeriesMatcher(
            Config config,
            DistanceCalculator distanceCalculator)
    {
        this.config = config;
        this.distanceCalculator = distanceCalculator;
    }

    public List<CityData> match(List<City> cities, List<DataSeries> allDataSeries)
    {
        // not an obvious way of doing this efficiently unless i build some kind of location-based
        // lookup structure. try brute force first. slow as multiple series
        System.out.println("Matching " + cities.size() + " cities with " + allDataSeries.size() + " data series");

        // group all the data series by station
        List<StationDataSeries> allStationDataSeries = this.groupStationDataSeries(allDataSeries);

        // relevant distances
        double maximumDistance = this.config.getMaximumCityStationDistanceInKm();
        double preferredDistance = this.config.getPreferredCityStationDistanceInKm();

        // process each city
        List<CityData> results = new ArrayList<CityData>();
        for (City city : cities)
        {
            // test filter to GBR
            //if (!city.getCountry().getIsoNameLong().equals("GBR"))
            //{
//                continue;
//            }

            // for each data type we want to fiond the closest data series unless there is a better one and both are
            // within the preferred distance. in this case we take the one build from the most amount of data points
            String cityDescription = city.getCountry().getName() + " - " + city.getName();
            System.out.println(cityDescription);
            Position cityPosition = city.getPosition();
            HashMap<DataSeriesType, CityDataSeries> bestDataSeriesMap = new HashMap<DataSeriesType, CityDataSeries>();

            // go through each station and see how close it is to this city
            for (StationDataSeries stationDataSeries : allStationDataSeries)
            {
                Station station = stationDataSeries.getStation();
                double newDistanceInKm = this.distanceCalculator.calculateDistanceInKm(cityPosition, station.getPosition());

                if (newDistanceInKm > maximumDistance)
                {
                    // this station is too far from the city
                    continue;
                }

                // iterate over all of the data series and see if any are closer or better (if close enough)
                for (DataSeries newDataSeries : stationDataSeries.getDataSeriesList())
                {
                    DataSeriesType type = newDataSeries.getType();
                    CityDataSeries existingCityDataSeries = bestDataSeriesMap.get(type);
                    if (existingCityDataSeries == null)
                    {
                        // new data type so just add
                        //System.out.println("  " + type + " => new series (new: " + newDistanceInKm + "km)");
                        bestDataSeriesMap.put(type, new CityDataSeries(city, newDataSeries, newDistanceInKm));
                        continue;
                    }

                    // have an existing series for this type. which is closer/better-if-close-enough
                    if (existingCityDataSeries.getDistanceInKm() > preferredDistance)
                    {
                        // existing is outside preferred distance so just take the new series if it's closer
                        if (newDistanceInKm < existingCityDataSeries.getDistanceInKm())
                        {
                            //System.out.println("  " + type + " =>   take new series (closer), old outside preferred (new: " + newDistanceInKm + "km, existing: " + existingCityDataSeries.getDistanceInKm() + "km)");
                            existingCityDataSeries.setDataSeries(newDataSeries);
                            existingCityDataSeries.setDistanceInKm(newDistanceInKm);
                        }
                        else
                        {
                            //System.out.println("  " + type + " =>   keep old series (closer), both outside preferred (new: " + newDistanceInKm + "km, existing: " + existingCityDataSeries.getDistanceInKm() + "km)");
                        }
                        continue;
                    }

                    // the existing data series is within the preferred distance. if the new one is outside the
                    // preferred distance then there's nothing to do
                    if (newDistanceInKm > preferredDistance)
                    {
                        //System.out.println("  " + type + " =>   keep old series, old inside, new outside (new: " + newDistanceInKm + "km, existing: " + existingCityDataSeries.getDistanceInKm() + "km)");
                        continue;
                    }

                    // both existing and new series are within the preferred distance so take the new one if it
                    // has a better data count
                    if (newDataSeries.getSourceCount() > existingCityDataSeries.getDataSeries().getSourceCount())
                    {
                        // new series is better but within preferred distance so we take it
                        //System.out.println(
                        //        "  " + type + " =>   both inside preferred, taking new " +
                        //        "(new: " + newDistanceInKm + "km, existing: " + existingCityDataSeries.getDistanceInKm() + "km), " +
                        //        "(new #: " + newDataSeries.getSourceCount() + ", existing #: " + existingCityDataSeries.getDataSeries().getSourceCount() + ")");
                        existingCityDataSeries.setDataSeries(newDataSeries);
                        existingCityDataSeries.setDistanceInKm(newDistanceInKm);
                    }
                    else
                    {
                        //System.out.println(
                        //        "  " + type + " =>   both inside preferred, keep existing " +
                        //        "(new: " + newDistanceInKm + "km, existing: " + existingCityDataSeries.getDistanceInKm() + "km), " +
                        //        "(new #: " + newDataSeries.getSourceCount() + ", existing #: " + existingCityDataSeries.getDataSeries().getSourceCount() + ")");
                    }
                }
            }

            // did we get any data within the max distance allowed?
            if (bestDataSeriesMap.size() == 0)
            {
                // no data
                //System.out.println("no data found close enough to city: " + cityDescription);
                //continue;
            }
            else
            {
                // if we get here then we have valid data for this city
                System.out.println("  Data found for " + bestDataSeriesMap.size() + " data types: " + cityDescription);
                results.add(new CityData(city, new ArrayList<CityDataSeries>(bestDataSeriesMap.values())));
            }
            System.out.println();
        }

        System.out.println("Matched " + results.size() + " cities to data series");
        return results;
    }

    private List<StationDataSeries> groupStationDataSeries(List<DataSeries> allDataSeries)
    {
        HashMap<String, StationDataSeries> stationDataSeriesMap = new HashMap<String, StationDataSeries>();
        for (DataSeries dataSeries : allDataSeries)
        {
            Station station = dataSeries.getStation();
            if (station.getPosition() == null)
            {
                System.out.println("Skipping station with no position data: " + station.getId() + " => " + station.getName());
                continue;
            }

            String stationId = station.getId();

            StationDataSeries stationDataSeries = stationDataSeriesMap.get(stationId);
            if (stationDataSeries == null)
            {
                stationDataSeries = new StationDataSeries(station);
                stationDataSeriesMap.put(stationId, stationDataSeries);
            }

            stationDataSeries.getDataSeriesList().add(dataSeries);
        }
        return new ArrayList(stationDataSeriesMap.values());
    }
}
