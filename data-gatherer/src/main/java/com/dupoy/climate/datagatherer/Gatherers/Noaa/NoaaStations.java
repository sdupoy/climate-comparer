package com.dupoy.climate.datagatherer.Gatherers.Noaa;

import com.dupoy.climate.datagatherer.Data.Source;
import com.dupoy.climate.datagatherer.Data.Station;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NoaaStations
{
    private List<Station> orderedList = new ArrayList<Station>();
    private HashMap<Integer, Station> data = new HashMap<Integer, Station>();

    public Station getStation(int id)
    {
        Station station = this.data.get(id);
        if (station == null)
        {
            String stringId = "noaa-" + id;
            station = new Station(Source.Noaa, stringId, stringId);
            this.data.put(id, station);
            this.orderedList.add(station);
        }
        return station;
    }

    public List<Station> getStations()
    {
        return this.orderedList;
    }
}
