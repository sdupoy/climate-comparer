package com.dupoy.climate.datagatherer;

public class GathererException extends RuntimeException
{
    public GathererException(String message)
    {
        super(message);
    }
}
