package com.dupoy.climate.datagatherer.Missing;

import com.dupoy.climate.datagatherer.Data.City;
//import com.dupoy.climate.datagatherer.Data.StationClimateData;

public interface IMissingLocation
{
    // can be null
    City getCity();

    // can be null
    //StationClimateData getStationClimateData();
}
