package com.dupoy.climate.datagatherer.Data;

public class CityDataSeries
{
    private final City city;
    private DataSeries dataSeries;
    private double distanceInKm;

    public CityDataSeries(City city, DataSeries dataSeries, double distanceInKm)
    {
        this.city = city;
        this.dataSeries = dataSeries;
        this.distanceInKm = distanceInKm;
    }

    public City getCity()
    {
        return city;
    }

    public DataSeries getDataSeries()
    {
        return dataSeries;
    }

    public void setDataSeries(DataSeries dataSeries)
    {
        this.dataSeries = dataSeries;
    }

    public double getDistanceInKm()
    {
        return distanceInKm;
    }

    public void setDistanceInKm(double distanceInKm)
    {
        this.distanceInKm = distanceInKm;
    }
}
