package com.dupoy.climate.datagatherer.Gatherers.Noaa;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.DataSeries;
import com.dupoy.climate.datagatherer.Data.DataSeriesType;
import com.dupoy.climate.datagatherer.Download.Mappings.IMapping;
import com.google.inject.Inject;
import ucar.ma2.ArrayByte;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayShort;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class NoaaFileReader
{
    private final Config config;
    private final NoaaStations noaaStations;

    @Inject
    public NoaaFileReader(
            Config config,
            NoaaStations noaaStations)
    {
        this.config = config;
        this.noaaStations = noaaStations;
    }

    public void retrieve1DFloats(IMapping mappingFile, String id, Consumer<NoaaDataPair<Float>> valueSetter)
            throws IOException
    {
        String filePath = mappingFile.getDestinationFile().getPath();
        System.out.println("reading: " + filePath);
        NetcdfFile file = NetcdfFile.open(filePath);

        // get the station ids + data
        Variable stationIdsVariable = file.findVariable("WBAN");
        Variable dataVariable = file.findVariable(id);

        // assume dimensions are the same
        int count = stationIdsVariable.getShape(0);
        ArrayInt.D1 stationIds = (ArrayInt.D1)stationIdsVariable.read();
        ArrayFloat.D1 data = (ArrayFloat.D1)dataVariable.read();

        Float missingValue = this.getFloatAttribute(dataVariable, "missing_value");
        Float scalingFactor = this.getFloatAttribute(dataVariable, "scale_factor");
        if (scalingFactor != null)
        {
            throw new IOException("Unexpected scale_factor for 1D noaa data source: " + id);
        }

        for (int i = 0; i < count; i++)
        {
            int stationId = stationIds.get(i);
            float value = data.get(i);
            // TODO: scale if necessary
            if (missingValue == null || value != missingValue)
            {
                valueSetter.accept(new NoaaDataPair(this.noaaStations.getStation(stationId), value));
            }
        }
    }

    public void retrieve1DIntegers(IMapping mappingFile, String id, Consumer<NoaaDataPair<Integer>> valueSetter)
            throws IOException
    {
        String filePath = mappingFile.getDestinationFile().getPath();
        System.out.println("reading: " + filePath);
        NetcdfFile file = NetcdfFile.open(filePath);

        Variable stationIdsVariable = file.findVariable("WBAN");
        Variable dataVariable = file.findVariable(id);

        //this.InspectVariable(stationIdsVariable);
        //this.InspectVariable(dataVariable);

        // assume dimensions are the same
        int count = stationIdsVariable.getShape(0);
        ArrayInt.D1 stationIds = (ArrayInt.D1)stationIdsVariable.read();
        ArrayInt.D1 data = (ArrayInt.D1)dataVariable.read();

        Integer missingValue = this.getIntAttribute(dataVariable, "missing_value");
        Float scalingFactor = this.getFloatAttribute(dataVariable, "scale_factor");
        if (scalingFactor != null)
        {
            throw new IOException("Unexpected scale_factor for 1D noaa data source: " + id);
        }

        for (int i = 0; i < count; i++)
        {
            int stationId = stationIds.get(i);
            int value = data.get(i);
            // TODO: scale if necessary
            if (missingValue == null || value != missingValue)
            {
                valueSetter.accept(new NoaaDataPair(this.noaaStations.getStation(stationId), value));
            }
        }
    }

    public List<DataSeries> retrieve2DShorts(IMapping mappingFile, String id, DataSeriesType type)
            throws IOException
    {
        String filePath = mappingFile.getDestinationFile().getPath();
        System.out.println("opening: " + filePath);
        NetcdfFile file = NetcdfFile.open(filePath);

        Variable stationIdsVariable = file.findVariable("WBAN");
        Variable timeVariable = file.findVariable("T");
        Variable dataVariable = file.findVariable(id);

        //this.Inspect(id, file);
        //this.InspectVariable(dataVariable);

        Integer missingValue = this.getIntAttribute(dataVariable, "missing_value");
        Float scalingFactor = this.getFloatAttribute(dataVariable, "scale_factor");

        // assume dimensions are the same
        ArrayInt.D1 stationIds = (ArrayInt.D1)stationIdsVariable.read();
        ArrayShort.D2 data = (ArrayShort.D2)dataVariable.read();

        String name = this.getStringAttribute(dataVariable, "long_name");
        String units = this.getStringAttribute(dataVariable, "units");

        int[] monthIndices = this.getMonthIndices(timeVariable);

        int stationCount = stationIdsVariable.getShape(0);
        int stationCountWithData = 0;
        int dateCount = monthIndices.length;
        System.out.println("  reading data: stationCount: " + stationCount + ", dateCount: " + dateCount);

        // should probably use iterators
        List<DataSeries> results = new ArrayList<DataSeries>();
        for (int stationIndex = 0; stationIndex < stationCount; stationIndex++)
        {
            int stationId = stationIds.get(stationIndex);
            //System.out.println("    reading " + id + " data for stationId " + stationId);

            // temp containers
            long[] monthlySums = new long[12]; // zero-initialised
            int[] monthlyCounts = new int[12]; // zero-initialised

            // pull out the values
            for (int dateIndex = 0; dateIndex < dateCount; dateIndex++)
            {
                short value = data.get(stationIndex, dateIndex);
                if (value == missingValue)
                {
                    continue;
                }
                int monthIndex = monthIndices[dateIndex];
                monthlySums[monthIndex] += value;
                monthlyCounts[monthIndex]++;
            }

            Float[] averages = new Float[12]; // null-initialised
            boolean hasValues = false;
            int sourceCount = 0;
            for (int sumIndex = 0; sumIndex < 12; sumIndex++)
            {
                long sum = monthlySums[sumIndex];
                int sumCount = monthlyCounts[sumIndex];

                if (sumCount >= this.config.getNoaaMinimumDailyDataCountPerMonth())
                {
                    Float avg = (float) sum / sumCount;
                    if (scalingFactor != null)
                    {
                        avg *= scalingFactor;
                    }
                    averages[sumIndex] = avg;
                    sourceCount += sumCount;
                    hasValues = true;
                }
            }

            // pack everything up into a MonthlyData structure
            if (hasValues)
            {
                results.add(new DataSeries(type, this.noaaStations.getStation(stationId), averages, sourceCount));
                stationCountWithData++;
            }
        }

        System.out.println("  read data for " + stationCountWithData + " / " + stationCount + " stations");
        return results;
    }

    public List<DataSeries> retrieve2DBytes(IMapping mappingFile, String id, DataSeriesType type)
            throws IOException
    {
        String filePath = mappingFile.getDestinationFile().getPath();
        System.out.println("opening: " + filePath);
        NetcdfFile file = NetcdfFile.open(filePath);

        Variable stationIdsVariable = file.findVariable("WBAN");
        Variable timeVariable = file.findVariable("T");
        Variable dataVariable = file.findVariable(id);

        //this.Inspect(id, file);
        //this.InspectVariable(dataVariable);

        Integer missingValue = this.getIntAttribute(dataVariable, "missing_value");
        Float scalingFactor = this.getFloatAttribute(dataVariable, "scale_factor");

        // assume dimensions are the same
        ArrayInt.D1 stationIds = (ArrayInt.D1)stationIdsVariable.read();
        ArrayByte.D2 data = (ArrayByte.D2)dataVariable.read();

        String name = this.getStringAttribute(dataVariable, "long_name");
        String units = this.getStringAttribute(dataVariable, "units");

        int[] monthIndices = this.getMonthIndices(timeVariable);

        int stationCount = stationIdsVariable.getShape(0);
        int stationCountWithData = 0;
        int dateCount = monthIndices.length;
        System.out.println("  reading data: stationCount: " + stationCount + ", dateCount: " + dateCount);

        // should probably use iterators
        List<DataSeries> results = new ArrayList<DataSeries>();
        for (int stationIndex = 0; stationIndex < stationCount; stationIndex++)
        {
            int stationId = stationIds.get(stationIndex);
            //System.out.println("    reading " + id + " data for stationId " + stationId);

            // temp containers
            long[] monthlySums = new long[12]; // zero-initialised
            int[] monthlyCounts = new int[12]; // zero-initialised

            // pull out the values
            for (int dateIndex = 0; dateIndex < dateCount; dateIndex++)
            {
                short value = data.get(stationIndex, dateIndex);
                if (value == missingValue)
                {
                    continue;
                }
                int monthIndex = monthIndices[dateIndex];
                monthlySums[monthIndex] += value;
                monthlyCounts[monthIndex]++;
            }

            Float[] averages = new Float[12]; // null-initialised
            boolean hasValues = false;
            int sourceCount = 0;
            for (int sumIndex = 0; sumIndex < 12; sumIndex++)
            {
                long sum = monthlySums[sumIndex];
                int sumCount = monthlyCounts[sumIndex];

                if (sumCount >= this.config.getNoaaMinimumDailyDataCountPerMonth())
                {
                    Float avg = (float) sum / sumCount;
                    if (scalingFactor != null)
                    {
                        avg *= scalingFactor;
                    }
                    averages[sumIndex] = avg;
                    sourceCount += sumCount;
                    hasValues = true;
                }
            }

            // pack everything up into a MonthlyData structure
            if (hasValues)
            {
                results.add(new DataSeries(type, this.noaaStations.getStation(stationId), averages, sourceCount));
                stationCountWithData++;
            }
        }

        System.out.println("  read data for " + stationCountWithData + " / " + stationCount + " stations");
        return results;
    }

    private int[] getMonthIndices(Variable timeVariable)
            throws IOException
    {
        int count = timeVariable.getShape(0);
        int offset = this.getIntAttribute(timeVariable, "add_offset"); // null check
        ArrayInt.D1 dates = (ArrayInt.D1)timeVariable.read();
        int[] monthIndices = new int[count];
        for (int i = 0; i < count; i++)
        {
            int date = dates.get(i);
            int julianDate = date + offset;
            int monthIndex = this.getMonthIndex(julianDate);
            monthIndices[i] = monthIndex;
        }
        return monthIndices;
    }

    private int getMonthIndex(int julianDay)
    {
        // https://en.wikipedia.org/wiki/Julian_day#Julian_or_Gregorian_calendar_from_Julian_day_number
        int x = (4 * julianDay + 274277) / 146097;
        int y = (x * 3) / 4;
        int f = julianDay + 1401 + y - 38;
        int e = 4 * f + 3;
        int g = (e % 1461) / 4;
        int h = 5 * g + 2;
        int z = h / 153;
        int m = (z + 2) % 12;
        return m;
    }

    private void Inspect(String id, NetcdfFile file)
    {
        System.out.println("file id: " + id);
        for (Dimension dimension : file.getDimensions())
        {
            System.out.println("  dimension: " + dimension);
        }
        for (Variable variable : file.getVariables())
        {
            this.InspectVariable(variable);
        }
    }

    private void InspectVariable(Variable variable)
    {
        System.out.println("  variable: " + variable.getFullNameEscaped());
        for (int s : variable.getShape())
        {
            System.out.println("    shape: " + s);
        }
        for (Attribute a : variable.getAttributes())
        {
            System.out.println("    attribute: " + a);
        }
        System.out.println("    isNumeric:       " + variable.getDataType().isNumeric());
        System.out.println("    isIntegral:      " + variable.getDataType().isIntegral());
        System.out.println("    isFloatingPoint: " + variable.getDataType().isFloatingPoint());
    }

    private Integer getIntAttribute(Variable variable, String key)
    {
        for (Attribute attribute : variable.getAttributes())
        {
            if (attribute.getFullName().equals(key))
            {
                System.out.println("  " + key + ": " + attribute + " (Integer)");
                return attribute.getNumericValue(0).intValue();
            }
        }
        return null;
    }

    private Float getFloatAttribute(Variable variable, String key)
    {
        for (Attribute attribute : variable.getAttributes())
        {
            if (attribute.getFullName().equals(key))
            {
                System.out.println("  " + key + ": " + attribute + " (Float)");
                return attribute.getNumericValue(0).floatValue();
            }
        }
        return null;
    }

    private String getStringAttribute(Variable variable, String key)
    {
        for (Attribute attribute : variable.getAttributes())
        {
            if (attribute.getFullName().equals(key))
            {
                System.out.println("  " + key + ": " + attribute + " (String)");
                return attribute.getStringValue();
            }
        }
        return null;
    }
}
