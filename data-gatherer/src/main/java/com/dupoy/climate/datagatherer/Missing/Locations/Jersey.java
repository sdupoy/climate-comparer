package com.dupoy.climate.datagatherer.Missing.Locations;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.*;
import com.dupoy.climate.datagatherer.Missing.IMissingLocation;

public class Jersey implements IMissingLocation
{
    private final float[] averageTemperatures = new float[] { 6, 6, 8,  9, 12, 14, 17, 17, 16, 13,  9, 7 };
    private final float[] minimumTemperatures = new float[] { 4, 4, 6,  6,  9, 12, 14, 14, 13, 11,  8, 6 };
    private final float[] maximumTemperatures = new float[] { 7, 7, 9, 11, 14, 16, 19, 19, 18, 14, 10, 8 };

    private final City city;
    //private final StationClimateData stationClimateData;

    public Jersey(Config config)
    {
        Double latitude = 49.205967;
        Double longitude = -2.195769;
        int elevation = 84;

        Country country = new Country("United Kingdom", "GB", "GBR");
        this.city = new City("Jersey", "Jersey", new Position(latitude, longitude), 100000, country, "Channel Islands");

        Station station = new Station(Source.Manual, "Jersey Airport", "Jersey Airport");
        station.setLatitude(latitude);
        station.setLongitude(longitude);
        station.setElevation(elevation);

        /*
        this.stationClimateData = new StationClimateData(config, station);
        this.stationClimateData.getAverageTemperatures().addData(this.averageTemperatures);
        this.stationClimateData.getMinimumTemperatures().addData(this.minimumTemperatures);
        this.stationClimateData.getMaximumTemperatures().addData(this.maximumTemperatures);
        */
    }

    public City getCity()
    {
        return this.city;
    }

    /*
    public StationClimateData getStationClimateData()
    {
        return this.stationClimateData;
    }
    */
}
