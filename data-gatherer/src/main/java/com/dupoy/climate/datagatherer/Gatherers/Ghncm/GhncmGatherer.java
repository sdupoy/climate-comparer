package com.dupoy.climate.datagatherer.Gatherers.Ghncm;

import com.dupoy.climate.datagatherer.Config.Config;
import com.dupoy.climate.datagatherer.Data.DataSeries;
import com.dupoy.climate.datagatherer.Data.DataSeriesType;
import com.dupoy.climate.datagatherer.Data.Station;
import com.dupoy.climate.datagatherer.Download.Mappings.IMapping;
import com.dupoy.climate.datagatherer.GathererException;
import com.dupoy.climate.datagatherer.Parsers.Ghncm.TemperatureDataRow;
import com.dupoy.climate.datagatherer.Parsers.Ghncm.TemperatureFileParser;
import com.dupoy.climate.datagatherer.Parsers.StationFileParser;
import com.google.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GhncmGatherer
{
    private final Config config;
    private final StationFileParser stationFileParser;
    private final TemperatureFileParser temperatureFileParser;

    @Inject
    public GhncmGatherer(
            Config config,
            StationFileParser stationFileParser,
            TemperatureFileParser temperatureFileParser)
    {
        this.config = config;
        this.stationFileParser = stationFileParser;
        this.temperatureFileParser = temperatureFileParser;
    }

    public List<DataSeries> gather()
            throws IOException
    {
        System.out.println("GhncmGatherer.gather()");
        HashMap<String, Station> stationMap = this.getStationMap();
        List<DataSeries> data = new ArrayList<DataSeries>();
        System.out.println("TODO: GHNCM READS BACK IN");
        //data.addAll(this.processFile(stationMap, this.config.getAverageTemperatureDataSource(), DataSeriesType.AverageTemperature));
        //data.addAll(this.processFile(stationMap, this.config.getMinimumTemperatureDataSource(), DataSeriesType.MinimumTemperature));
        //data.addAll(this.processFile(stationMap, this.config.getMaximumTemperatureDataSource(), DataSeriesType.MaximumTemperature));
        return data;
    }

    private HashMap<String, Station> getStationMap()
            throws IOException
    {
        List<Station> stations = this.stationFileParser.parse(this.config.getStationDataSource().getDestinationFile());
        HashMap<String, Station> stationMap = new HashMap<String, Station>();
        for (Station station : stations)
        {
            stationMap.put(station.getId(), station);
        }
        return stationMap;
    }

    private List<DataSeries> processFile(HashMap<String, Station> stationMap, IMapping mapping, DataSeriesType type)
            throws IOException
    {
        // get rows
        List<TemperatureDataRow> rows = this.temperatureFileParser.parseAndFilter(mapping.getDestinationFile());

        // gather rows by station
        HashMap<String, MonthlyDataSummer> summers = new HashMap<String, MonthlyDataSummer>();
        for (TemperatureDataRow row : rows)
        {
            MonthlyDataSummer summer = summers.get(row.getStationId());
            if (summer == null)
            {
                summer = new MonthlyDataSummer(this.config);
                summers.put(row.getStationId(), summer);
            }
            summer.addData(row.getData());
        }

        // go through each MonthlyDataSummer. if it has enough data then find its station
        // and build a DataSeries object
        List<DataSeries> results = new ArrayList<DataSeries>();
        for (Map.Entry<String, MonthlyDataSummer> entry : summers.entrySet())
        {
            // unpack the set entry
            String stationId = entry.getKey();
            MonthlyDataSummer summer = entry.getValue();

            // enough data?
            if (!summer.isValid())
            {
                continue;
            }

            // get the station
            Station station = stationMap.get(stationId);
            if (station == null)
            {
                throw new GathererException("Cannot process temperature data for unknown station id: " + stationId);
            }

            results.add(new DataSeries(
                    type,
                    station,
                    summer.getData(),
                    summer.getCount()));
        }

        return results;
    }
}
