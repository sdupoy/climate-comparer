package com.dupoy.climate.datagatherer.Download.Mappings;

import com.dupoy.climate.datagatherer.Config.Config;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import java.io.*;
import java.net.URL;

public class CompressedMapping
    extends SimpleMapping
{
    public CompressedMapping(Config config, String sourcePath, String destinationFileName)
    {
        super(config, sourcePath, destinationFileName);
    }

    public void DownloadAndUnpack()
            throws IOException
    {
        // don't do anything if the destination file already exists
        if (this.destinationFile.exists())
        {
            System.out.println("Destination file already present: " + this.destinationFile.getPath());
            return;
        }

        // download the tar.gz file
        File zippedFile = this.getZippedFilePath();
        super.Download(this.sourcePath, zippedFile);

        // now unzip and extract from tar
        File unzippedFile = this.Unzip(zippedFile);
        this.ExtractFromTar(unzippedFile);
    }

    private File getZippedFilePath()
            throws IOException
    {
        return new File(this.config.getTempDirectoryPath(), this.getSourceFileName());
    }

    private File getUnzippedFilePath()
            throws IOException
    {
        String sourceFileName = this.getSourceFileName();
        String decompressedFileName = sourceFileName.replace(".tar.gz", ".tar");
        return new File(this.config.getTempDirectoryPath(), decompressedFileName);
    }

    private String getSourceFileName()
            throws IOException
    {
        String path = new URL(this.sourcePath).getPath();
        return path.substring(path.lastIndexOf('/') + 1, path.length());
    }

    private File Unzip(File zippedFile)
            throws IOException
    {
        System.out.println("Unzipping file: " + zippedFile.getPath());
        File unzippedFile = this.getUnzippedFilePath();
        if (unzippedFile.exists())
        {
            System.out.println("File already unzipped: " + unzippedFile.getPath());
            return unzippedFile;
        }

        FileOutputStream outputFileStream = null;
        GzipCompressorInputStream gzipInputStream = null;
        try
        {
            FileInputStream fileInputStream = new FileInputStream(zippedFile);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            outputFileStream = new FileOutputStream(unzippedFile);
            gzipInputStream = new GzipCompressorInputStream(bufferedInputStream);
            final byte[] buffer = new byte[4096];
            int n = 0;
            while ((n = gzipInputStream.read(buffer)) != -1)
            {
                outputFileStream.write(buffer, 0, n);
            }
        }
        finally
        {
            if (outputFileStream != null)
            {
                outputFileStream.close();
            }
            if (gzipInputStream != null)
            {
                gzipInputStream.close();
            }
        }

        System.out.println("Unzipped to: " + unzippedFile.getPath());
        return unzippedFile;
    }

    private void ExtractFromTar(File unzippedFile)
            throws IOException
    {
        System.out.println("Extract data file from .tar file");
        TarArchiveInputStream tarInputStream = null;
        FileOutputStream fileOutputStream = null;
        try
        {
            // find the tar entry we want to pull out of the .tar
            tarInputStream = new TarArchiveInputStream(new FileInputStream(unzippedFile));
            TarArchiveEntry tarEntry = tarInputStream.getNextTarEntry();
            TarArchiveEntry dataFileTarEntry = null;
            while (tarEntry != null)
            {
                if (tarEntry.getName().endsWith(".dat"))
                {
                    dataFileTarEntry = tarEntry;
                    break;
                }
                tarEntry = tarInputStream.getNextTarEntry();
            }
            if (dataFileTarEntry == null)
            {
                throw new IOException("Couldn't find .dat tar entry in tar file: " + unzippedFile.getPath());
            }

            // got the right tar entry so extract the file. the input stream will be in the right place.
            System.out.println("Extract tar file " + dataFileTarEntry.getName() + " to " + this.destinationFile.getPath());
            fileOutputStream = new FileOutputStream(this.destinationFile);
            final byte[] buffer = new byte[4096];
            int n = 0;
            while ((n = tarInputStream.read(buffer)) != -1)
            {
                fileOutputStream.write(buffer, 0, n);
            }
        }
        finally
        {
            if (tarInputStream != null)
            {
                tarInputStream.close();
            }
            if (fileOutputStream != null)
            {
                fileOutputStream.close();
            }
        }
    }
}
