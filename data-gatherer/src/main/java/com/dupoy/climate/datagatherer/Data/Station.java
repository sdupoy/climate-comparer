package com.dupoy.climate.datagatherer.Data;

public class Station
{
    private final Source source;
    private final String id;
    private final String name;

    private Double latitude;
    private Double longitude;
    private Integer elevation;

    public Station(
            Source source,
            String id,
            String name)
    {
        this.source = source;
        this.id = id;
        this.name = name;
    }

    public Source getSource()
    {
        return source;
    }

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setLatitude(Double latitude)
    {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude)
    {
        this.longitude = longitude;
    }

    public void setElevation(Integer elevation)
    {
        this.elevation = elevation;
    }

    public Position getPosition()
    {
        if (this.latitude == null || this.longitude == null)
        {
            return null;
        }
        return new Position(this.latitude, this.longitude);
    }

    public Integer getElevation()
    {
        return this.elevation;
    }
}
