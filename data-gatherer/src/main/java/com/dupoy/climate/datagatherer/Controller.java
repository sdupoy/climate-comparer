package com.dupoy.climate.datagatherer;

import com.dupoy.climate.datagatherer.Data.CityData;
import com.dupoy.climate.datagatherer.Download.Downloader;
import com.dupoy.climate.datagatherer.Gatherers.Gatherer;
import com.dupoy.climate.datagatherer.Output.Writer;
import com.google.inject.Inject;

import java.io.IOException;
import java.util.List;

public class Controller
{
    private final Downloader downloader;
    private final Gatherer gatherer;
    private final Writer writer;

    @Inject
    public Controller(
            Downloader downloader,
            Gatherer gatherer,
            Writer writer)
    {
        this.downloader = downloader;
        this.gatherer = gatherer;
        this.writer = writer;
    }

    public void Run()
            throws IOException
    {
        System.out.println("+Controller.Run()");
        //this.downloader.DownloadAndUnpack();
        List<CityData> data = this.gatherer.gather();
        //this.writer.write(data);
        System.out.println("-Controller.Run()");
    }
}
